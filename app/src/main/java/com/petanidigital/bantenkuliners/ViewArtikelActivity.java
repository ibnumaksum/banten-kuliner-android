package com.petanidigital.bantenkuliners;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

public class ViewArtikelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_artikel);
        WebView webView = findViewById(R.id.webView);
        TextView judul = findViewById(R.id.txtJudul);
        TextView penulis = findViewById(R.id.txtPenulis);
        Intent intent = getIntent();
        judul.setText(intent.getStringExtra("judul"));
        penulis.setText("Oleh "+intent.getStringExtra("penulis"));
        webView.loadData("<style> img {width:100%;height: auto;}</style> "+intent.getStringExtra("isi"),"text/html","UTF-8");
    }
}
