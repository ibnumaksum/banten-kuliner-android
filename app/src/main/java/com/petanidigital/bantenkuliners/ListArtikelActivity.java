package com.petanidigital.bantenkuliners;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.petanidigital.bantenkuliners.utils.HttpGET;
import com.petanidigital.bantenkuliners.utils.HttpGETcallback;
import com.petanidigital.bantenkuliners.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

public class ListArtikelActivity extends AppCompatActivity {

    int hal = 0;
    String cari = "";
    EditText txtCari;
    Button btnClear, btnNext,btnPrev;
    ArrayAdapter<JSONObject> adapter;
    private ListView mDrawerListView;
    private SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_artikel);
        setTitle("Artikel Kuliner Banten");
        txtCari = findViewById(R.id.txtCari);
        btnClear = findViewById(R.id.btnClear);
        btnNext = findViewById(R.id.btnNext);
        btnPrev = findViewById(R.id.btnPrev);
        txtCari.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(txtCari.getText().length()>0)
                    btnClear.setVisibility(View.VISIBLE);
                else
                    btnClear.setVisibility(View.GONE);
            }
        });

        mDrawerListView = findViewById(R.id.listVoew);
        swipe = findViewById(R.id.swipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtCari.setText("");
                hal = 0;
                getData();
            }
        });
        txtCari.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        // Identifier of the action. This will be either the identifier you supplied,
                        // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE
                                || event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            try{
                                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(txtCari.getWindowToken(), 0);
                            }catch (Exception e){
                                Utils.log(e.getMessage());
                            }
                            getData();
                            return true;
                        }
                        // Return true if you have consumed the action, else false.
                        return false;
                    }
                });
        adapter = new ArrayAdapter<JSONObject>(
                ListArtikelActivity.this,
                R.layout.item_artikel,
                R.id.txtJudul
        ) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView txtjudul = view.findViewById(R.id.txtJudul);
                try{
                    JSONObject json = getItem(position);
                    txtjudul.setText(json.getString("judul_artikel"));
                }catch (Exception e){
                    Utils.log(e.toString());
                }
                return view;
            }
        };
        mDrawerListView.setAdapter(adapter);
        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try{
                    JSONObject j = adapter.getItem(i);
                    Intent intent = new Intent(ListArtikelActivity.this,ViewArtikelActivity.class);
                    intent.putExtra("judul",j.getString("judul_artikel"));
                    intent.putExtra("penulis",j.getString("namaLengkap"));
                    intent.putExtra("isi",j.getString("isi_artikel"));
                    startActivity(intent);
                }catch (Exception e){
                    Utils.log(e.getMessage());
                }
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hal++;
                getData();
            }
        });
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hal==0) return;
                --hal;
                getData();
            }
        });
        swipe.post(new Runnable() {
            @Override
            public void run() {
                getData();
            }
        });
    }

    public void getData(){
        swipe.setRefreshing(true);
        cari = txtCari.getText().toString();
        HttpGET.doGetRequest(Utils.apiurl+"apa=artikel&hal="+hal+"&cari="+cari, new HttpGETcallback() {
            @Override
            public void httpResultSuccess(String result) {
                swipe.setRefreshing(false);
                parsingData(result);
            }

            @Override
            public void httpResultError(String result, int statusCode) {
                swipe.setRefreshing(false);
            }
        });
    }

    public void parsingData(String data){
        try{
            JSONArray array = new JSONArray(data);
            int jml = array.length();
            adapter.clear();

            for(int n=0;n<jml;n++){
                JSONObject json = array.getJSONObject(n);
                adapter.add(json);
            }
        }catch (Exception e){
            Utils.showToast("Gagal mendapatkan data dari server",this);
        }
    }

}
