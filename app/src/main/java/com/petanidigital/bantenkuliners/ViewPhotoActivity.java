package com.petanidigital.bantenkuliners;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ViewPhotoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_photo);


        PhotoView photoView = findViewById(R.id.photo_view);
        Intent intent = getIntent();
        Picasso.with(this).load(intent.getStringExtra("url")).placeholder(android.R.drawable.ic_menu_report_image).into(photoView);

    }

}
