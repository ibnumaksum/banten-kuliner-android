package com.petanidigital.bantenkuliners;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.petanidigital.bantenkuliners.utils.GetDataDialog;
import com.petanidigital.bantenkuliners.utils.HttpGET;
import com.petanidigital.bantenkuliners.utils.HttpGETcallback;
import com.petanidigital.bantenkuliners.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,LocationListener {
    MapView mapView;
    GoogleMap map;
    Float lat,lon;
    boolean isGPS, belumUser=true;
    int akurasi;
    private LocationManager locationManager;
    ArrayAdapter<JSONObject> adapter;
    private ListView mDrawerListView;
    private SwipeRefreshLayout swipe;
    LatLngBounds.Builder builder = new LatLngBounds.Builder();
    LatLng myloc;
    String kota;
    int hal = 0;
    Button btnClear;
    ImageButton btnNext, btnPrev;
    EditText txtCari;
    TextView txtInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();

        kota = intent.getStringExtra("kota");
        toolbar.setTitle("Banten Kuliner");
        toolbar.setSubtitle(kota.replace("%20"," "));

        mDrawerListView = (ListView)findViewById(R.id.listViewDefault);
        swipe = (SwipeRefreshLayout)findViewById(R.id.listViewSwipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    Utils.log("posisi: "+i);
                    JSONObject json = adapter.getItem(i-1);
                    Intent intent = new Intent(MainActivity.this,ViewRestoActivity.class);
                    intent.putExtra("data",json.toString());
                    if(myloc!=null) {
                        intent.putExtra("lat", myloc.latitude);
                        intent.putExtra("lon", myloc.longitude);
                    }
                    startActivity(intent);
                }catch (Exception e){
                    Utils.log(e.getMessage());
                }
            }
        });
        //Maps
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.map_layout, mDrawerListView,false);
        mapView = (MapView) view.findViewById(R.id.mapView);
        btnClear = view.findViewById(R.id.btnClear);
        txtCari = view.findViewById(R.id.txtCari);
        txtCari.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(txtCari.getText().length()>0)
                    btnClear.setVisibility(View.VISIBLE);
                else
                    btnClear.setVisibility(View.GONE);
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtCari.setText("");
                hal = 0;
                getData();
            }
        });
        txtCari.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        // Identifier of the action. This will be either the identifier you supplied,
                        // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE
                                || event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            try{
                                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(txtCari.getWindowToken(), 0);
                            }catch (Exception e){
                                Utils.log(e.getMessage());
                            }
                            getData();
                            return true;
                        }
                        // Return true if you have consumed the action, else false.
                        return false;
                    }
                });

        mDrawerListView.addHeaderView(view);

        View footer = inflater.inflate(R.layout.btn_next_prev, mDrawerListView,false);
        mDrawerListView.addFooterView(footer);
        btnNext = footer.findViewById(R.id.btnNext);
        btnPrev = footer.findViewById(R.id.btnPrev);
        txtInfo = footer.findViewById(R.id.txtInfo);
        txtInfo.setText("Halaman 1");
        btnPrev.setVisibility(View.INVISIBLE);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hal += 1;
                txtInfo.setText("Halaman "+(hal+1));
                getData();
                btnPrev.setVisibility(View.VISIBLE);
            }
        });
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hal -= 1;
                txtInfo.setText("Halaman "+(hal+1));
                getData();
                if(hal==0){
                    btnPrev.setVisibility(View.INVISIBLE);
                }
            }
        });

        //mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        swipe.post(new Runnable() {
            @Override
            public void run() {
                mapView.getMapAsync(MainActivity.this);
            }
        });



    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(this);
        this.map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mapView.onResume();
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-6.117140, 106.152306), 17));
        getData();
        if (ActivityCompat.checkSelfPermission(getApplicationContext()
                , android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        getLocation();
    }

    public void getLocation(){
        //if(!isStart) Toast.makeText(this,"mencari lokasi anda...",Toast.LENGTH_SHORT).show();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default
        if ( !locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER )) {
            Utils.showToast("Mohon Aktifkan GPS", this);
            Intent gpsOptionsIntent = new Intent(
                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(gpsOptionsIntent);
        }else {

            try {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            } catch (SecurityException e) {
                    Toast.makeText(this, "Tidak memiliki permisi untuk lokasi :(", Toast.LENGTH_SHORT).show();
                Utils.log("Location: no permission " + e.getMessage());
            } catch (Exception e) {
                    Toast.makeText(this, "Tidak memiliki permisi untuk lokasi :(", Toast.LENGTH_SHORT).show();
                Utils.log("Location: no permission " + e.getMessage());
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        locationManager.removeUpdates(this);
        if(Build.VERSION.SDK_INT >= 18 && location.isFromMockProvider()){
            Utils.showToast("Mohon jangan gunakan lokasi palsu, karena cinta palsu sudah cukup menyakitkan", this);
            isGPS = false;
        }else if(Utils.isMockSettingsON(this)){
            Utils.showToast("Mohon jangan gunakan lokasi palsu, karena cinta palsu sudah cukup menyakitkan", this);
            isGPS = false;
        }else {
            isGPS = true;
            myloc = new LatLng(location.getLatitude(), location.getLongitude());
            //map.animateCamera(CameraUpdateFactory.newLatLngZoom(myloc,17));
            if(belumUser){
                builder.include(myloc);
                LatLngBounds bounds = builder.build();
                int width = getResources().getDisplayMetrics().widthPixels;
                int height = (int) convertDpToPixel(200,MainActivity.this);
                int padding = (int) (width * 0.12); // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,width, height, padding);
                try {
                    map.animateCamera(cu);
                }catch (Exception e){
                    Utils.log("animateCamera: "+e.getMessage());
                }
                if(adapter!=null)
                    adapter.notifyDataSetChanged();
            }
            lat = (float)location.getLatitude();
            lon = (float)location.getLongitude();
            Utils.log("Location: " + lat + "," + lon);
        }
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    private boolean isRunning = false;
    public void getData(){
        if(isRunning){
            Utils.showToast("APlikasi masih mengambil data",this);
            return;
        }
        isRunning = true;
        //http://api.bantenid.com/bante nkuliner.json
        swipe.setRefreshing(true);
        HttpGET.doGetRequest(Utils.apiurl+"apa=kota&kota=" + kota+"&hal="+hal+"&cari="+txtCari.getText().toString(), new HttpGETcallback() {
            @Override
            public void httpResultSuccess(String hasil) {
                isRunning = false;
                swipe.setRefreshing(false);
                map.clear();
                try {
                    Utils.log(hasil);
                    JSONArray json = new JSONArray(hasil);
                    int jml = json.length();
                    JSONObject jsonO[] = new JSONObject[jml];
                    int a = 0;
                    for(int n=jml-1;n>-1;--n){
                        jsonO[a] = json.getJSONObject(n);
                        try{
                            Marker marker = map.addMarker(new MarkerOptions()
                                    .position(new LatLng(jsonO[a].getDouble("lat"),jsonO[a].getDouble("lon")))
                                    .title(jsonO[a].getString("nama")));
                            builder.include(marker.getPosition());
                        }catch (Exception e){
                            Utils.log(e.getMessage());
                        }

                        a++;
                    }
                    if(myloc!=null) {
                        builder.include(myloc);
                        belumUser = false;
                    }
                    LatLngBounds bounds = builder.build();
                    int width = getResources().getDisplayMetrics().widthPixels;
                    int height = (int) convertDpToPixel(200,MainActivity.this);
                    int padding = (int) (width * 0.12); // offset from edges of the map in pixels
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,width, height, padding);
                    map.animateCamera(cu);
                    adapter = new ArrayAdapter<JSONObject>(
                            MainActivity.this,
                            R.layout.konten_item,
                            R.id.txtNama,
                            jsonO
                    ) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            TextView nama = view.findViewById(R.id.txtNama);
                            TextView alamat = view.findViewById(R.id.txtAlamat);
                            TextView deskripsi = view.findViewById(R.id.txtDeskripsi);
                            ImageView gbr = view.findViewById(R.id.imgGambar);
                            TextView txtJarak = view.findViewById(R.id.txtJarak);
                            try{
                                JSONObject json = getItem(position);
                                nama.setText(json.getString("nama"));
                                alamat.setText(json.getString("alamatLengkap"));
                                deskripsi.setText(json.getString("deskripsiPendek"));
                                Utils.log(json.getString("foto_lokasi"));
                                Picasso.with(MainActivity.this)
                                        .load(Utils.imgUrl+json.getString("foto_lokasi"))
                                        .error(R.drawable.bg_gray)
                                        .placeholder(R.drawable.bg_gray)
                                        .resize(200,200)
                                        .centerCrop()
                                        .into(gbr);
                                if(myloc!=null) {
                                    txtJarak.setText(Utils.distFromTxt((float) myloc.latitude, (float) myloc.longitude,
                                            (float) json.getDouble("lat"), (float) json.getDouble("lon")));
                                }else{
                                    txtJarak.setText("");
                                }
                            }catch (Exception e){
                                Utils.log(e.toString());
                            }
                            return view;
                        }
                    };
                    mDrawerListView.setAdapter(adapter);

                } catch (Exception e) {
                    Utils.log(e.toString());
                }

            }

            @Override
            public void httpResultError(String result, int statusCode) {
                isRunning = false;
                swipe.setRefreshing(false);
                Utils.showToast("Gagal mendapatkan data dari server",MainActivity.this);
            }
        });
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

}
