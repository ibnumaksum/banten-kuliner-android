package com.petanidigital.bantenkuliners;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.petanidigital.bantenkuliners.utils.HttpGET;
import com.petanidigital.bantenkuliners.utils.HttpGETcallback;
import com.petanidigital.bantenkuliners.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class PropinsiActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener {
    SliderLayout slider;

    int maxWidth,maxHeight;
    String kotaPilihan;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_propinsi);
        progress = new ProgressDialog(this);
        progress.setTitle("Melakukan koneksi ke server");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                bukaKota();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        maxHeight = displayMetrics.heightPixels;
        maxWidth = displayMetrics.widthPixels;

        slider = (SliderLayout) findViewById(R.id.slider);
        slider.stopAutoCycle();
        slider.setDuration(5000);

        HttpGET.doGetRequest(Utils.apiurl+"apa=slide&"+System.currentTimeMillis(), new HttpGETcallback() {
            @Override
            public void httpResultSuccess(String result) {
                parsingSlide(result);
            }

            @Override
            public void httpResultError(String result, int statusCode) {

            }
        });
    }



    public void parsingSlide(String result){
        try {
            JSONArray json = new JSONArray(result);
            Utils.log(json.toString());
            int jml = json.length();
            int height = 50;
            slider.removeAllSliders();
            for (int n = 0; n < jml; n++) {
                JSONObject obj = json.getJSONObject(n);
                Utils.log(obj.toString());
                if (obj.has("judulGambar") && obj.getString("judulGambar").length()>2) {
                    String img = Utils.imgUrl+obj.getString("pathGambar");
                    TextSliderView textSliderView = new TextSliderView(PropinsiActivity.this);
                    textSliderView
                            .image(img)
                            //.setScaleType(BaseSliderView.ScaleType.CenterInside)
                            .setOnSliderClickListener(PropinsiActivity.this);
                    if(obj.has("judulGambar"))
                        textSliderView.description(obj.getString("judulGambar"));
                    textSliderView.bundle(new Bundle());
                    if (obj.has("urlGambar") && obj.getString("urlGambar").length()>2) {
                        textSliderView.getBundle().putString("url", obj.getString("urlGambar"));
                    } else {
                        textSliderView.getBundle().putString("url", img);
                    }
                    slider.addSlider(textSliderView);
                    int h = 292;
                    if (height < h) {
                        height = h;
                        float ratioBitmap = (float) 789 / (float) height;
                        float ratioMax = (float) maxWidth / (float) maxHeight;
                        int finalWidth = maxWidth;
                        int finalHeight = maxHeight;
                        if (ratioMax > 1) {
                            finalWidth = (int) ((float) maxHeight * ratioBitmap);
                        } else {
                            finalHeight = (int) ((float) maxWidth / ratioBitmap);
                        }
                        height = finalHeight;
                    }
                } else {
                    String img =  Utils.imgUrl+obj.getString("pathGambar");
                    DefaultSliderView textSliderView = new DefaultSliderView(PropinsiActivity.this);
                    textSliderView
                            .image(img)
                            //.setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                            .setOnSliderClickListener(PropinsiActivity.this);
                    Bundle bundle = new Bundle();
                    if (obj.has("urlGambar") && obj.getString("urlGambar").length()>2) {
                        bundle.putString("url", obj.getString("urlGambar"));
                    } else {
                        bundle.putString("url", img);
                    }
                    textSliderView.bundle(bundle);
                    slider.addSlider(textSliderView);
                    int h = 292;
                    if (height < h) {
                        height = h;
                        float ratioBitmap = (float) 789 / (float) height;
                        float ratioMax = (float) maxWidth / (float) maxHeight;
                        int finalWidth = maxWidth;
                        int finalHeight = maxHeight;
                        if (ratioMax > 1) {
                            finalWidth = (int) ((float) maxHeight * ratioBitmap);
                        } else {
                            finalHeight = (int) ((float) maxWidth / ratioBitmap);
                        }
                        height = finalHeight;
                    }
                }
                if(jml>0) {
                    Utils.log("height " + height);
                    slider.getLayoutParams().height = height;
                    slider.setPresetTransformer(SliderLayout.Transformer.Stack);
                    slider.setPresetIndicator(SliderLayout.PresetIndicators.Right_Bottom);
                    // slider.setCustomAnimation(new DescriptionAnimation());
                    slider.setDuration(5000);
                    slider.startAutoCycle();
                }else{
                    slider.stopAutoCycle();
                    slider.setVisibility(View.GONE);
                }
            }
        }catch (Exception e){
            Utils.log(e.getMessage());
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        if (slider.getBundle() != null && slider.getBundle().containsKey("url")) {
            String url = slider.getBundle().getString("url");
            startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(url.trim())));
        }else{
            Utils.log("kosong");
        }
    }

    @Override
    public void onPause() {
        if(slider!=null)
            slider.stopAutoCycle();
        super.onPause();
    }

    @Override
    public void onResume() {
        if(progress!=null)
            progress.dismiss();
        if(slider!=null)
            slider.startAutoCycle();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        progress.dismiss();
        super.onDestroy();
    }

    public void bukaSerang(View view){
        bukaMain("KOTA%20SERANG");
    }

    public void bukaKabSerang(View view){
        bukaMain("KABUPATEN%20SERANG");
    }

    public void bukaCilegon(View view){
        bukaMain("KOTA%20CILEGON");
    }

    public void bukaPandeglang(View view){
        bukaMain("KABUPATEN%20PANDEGLANG");
    }

    public void bukaLebak(View view){
        bukaMain("KABUPATEN%20LEBAK");
    }

    public void bukaTangerang(View view){
        bukaMain("KOTA%20TANGERANG");
    }

    public void bukaTangsel(View view){
        bukaMain("KOTA%20TANGERANG%20SELATAN");
    }

    public void bukaKabTangerang(View view){
        bukaMain("KABUPATEN%20TANGERANG");
    }

    public void bukaArtikel(View view){
        //progress.show();
        startActivity(new Intent(this,ListArtikelActivity.class));
    }

    public void bukaMain(final String kota){
        progress.show();
        kotaPilihan = kota;


    }

    public void daftarSekarang(View view){
        startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(Utils.regurl)));
    }

    public void bukaKota(){
        Utils.log(kotaPilihan);
        Intent intent = new Intent(PropinsiActivity.this,MainActivity.class);
        intent.putExtra("kota",kotaPilihan);
        startActivity(intent);
        Utils.log(kotaPilihan+" Terbuka");
    }
}
