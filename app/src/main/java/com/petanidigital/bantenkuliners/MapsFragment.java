package com.petanidigital.bantenkuliners;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.petanidigital.bantenkuliners.utils.Utils;


public class MapsFragment extends BottomSheetDialogFragment implements OnMapReadyCallback,LocationListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    MapView mapView;
    GoogleMap map;
    LatLngBounds.Builder builder = new LatLngBounds.Builder();
    private LocationManager locationManager;
    double lat,lon,latt,lonn;
    LatLng myloc;

    public MapsFragment() {
        // Required empty public constructor
    }


    public static MapsFragment newInstance(String param1, String param2) {
        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        String[] latlon = mParam1.split("<lokasi>");
        latt = Double.parseDouble(latlon[0]);
        lonn = Double.parseDouble(latlon[1]);

        mapView = (MapView) view.findViewById(R.id.mapView2);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        MapsInitializer.initialize(getContext());

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mapView.onResume();
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latt, lonn), 17));
        Marker marker = map.addMarker(new MarkerOptions()
                .position(new LatLng(latt,lonn))
                .title(mParam2));
        builder.include(marker.getPosition());
        if (ActivityCompat.checkSelfPermission(getContext()
                , android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        //getLocation();
    }

    public void getLocation(){
        //if(!isStart) Toast.makeText(this,"mencari lokasi anda...",Toast.LENGTH_SHORT).show();
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default
        if ( !locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER )) {
            Utils.showToast("Mohon Aktifkan GPS", getContext());
            Intent gpsOptionsIntent = new Intent(
                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(gpsOptionsIntent);
        }else {

            try {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            } catch (SecurityException e) {
                Toast.makeText(getContext(), "Tidak memiliki permisi untuk lokasi :(", Toast.LENGTH_SHORT).show();
                Utils.log("Location: no permission " + e.getMessage());
            } catch (Exception e) {
                Toast.makeText(getContext(), "Tidak memiliki permisi untuk lokasi :(", Toast.LENGTH_SHORT).show();
                Utils.log("Location: no permission " + e.getMessage());
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        locationManager.removeUpdates(this);
        if(Build.VERSION.SDK_INT >= 18 && location.isFromMockProvider()){
            Utils.showToast("Mohon jangan gunakan lokasi palsu, karena cinta palsu sudah cukup menyakitkan", getContext());
        }else if(Utils.isMockSettingsON(getContext())){
            Utils.showToast("Mohon jangan gunakan lokasi palsu, karena cinta palsu sudah cukup menyakitkan", getContext());
        }else {
            myloc = new LatLng(location.getLatitude(), location.getLongitude());
            //map.animateCamera(CameraUpdateFactory.newLatLngZoom(myloc,17));
            builder.include(myloc);
            LatLngBounds bounds = builder.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = 200;
            int padding = (int) (width * 0.12); // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,width, height, padding);
            try {
                map.animateCamera(cu);
            }catch (Exception e){
                Utils.log("animateCamera: "+e.getMessage());
            }
            lat = (float)location.getLatitude();
            lon = (float)location.getLongitude();
            Utils.log("Location: " + lat + "," + lon);
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }
}
