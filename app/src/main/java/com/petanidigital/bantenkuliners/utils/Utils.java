package com.petanidigital.bantenkuliners.utils;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by ibnumaksum on 12/11/14.
 */
public class Utils {

    public static String regurl = "http://spot.untirta.ac.id/bakul/";
    public static String apiurl = "http://spot.untirta.ac.id/bakul/api.php?";
    public static String imgUrl = "http://spot.untirta.ac.id/bakul/";


    private static String tmp_kegiatan = "{\n" +
            "    \"id_cool\": \"\",\n" +
            "    \"id_korwil\": \"\",\n" +
            "    \"tanggal_kegiatan\": 0,\n" +
            "    \"persembahan_kegiatan\": 0,\n" +
            "    \"persembahan_gereja\": 0,\n" +
            "    \"gembala_kegiatan\": \":\",\n" +
            "    \"penilik_kegiatan\": \":\",\n" +
            "    \"kubu_doa\": \"N\",\n" +
            "    \"imam_musik\": \":\",\n" +
            "    \"pembawa_sharing_firman\": \":\",\n" +
            "    \"koordinator_doa\": \":\",\n" +
            "    \"pemimpin_pujian\": \":\",\n" +
            "    \"komentar_kegiatan\": \"\",\n" +
            "    \"jemaat_kegiatan\": [],\n" +
            "    \"tamu_kegiatan\": []\n" +
            "}";
    private static String hadir = "{" +
            "\"id\":\"1\",\n" +
            "\"nama\":\"ibnux\",\n" +
            "\"status\":\"hadir\"\n" +
            "}";
    private static String tamu = "{\n" +
            "\"kode\":\"123444\",\n" +
            "\"nama\":\"xnuqi\",\n" +
            "\"email\":\"\",\n" +
            "\"hp\":\"\",\n" +
            "\"tambah\":\"yes\"\n" +
            "}";

    public static JSONObject getTemplateKegiatan(String idCool, String idKorwil, int tgl){
        try {
            JSONObject json = new JSONObject(tmp_kegiatan);
            json = json.put("id_cool",idCool);
            json = json.put("id_korwil",idKorwil);
            json = json.put("tanggal_kegiatan",tgl);
            return json;
        }catch (Exception e){
            return new JSONObject();
        }
    }

    public static JSONObject putTamu(String kode, String nama, String email, String hp, boolean tambah){
        try {
            JSONObject json = new JSONObject(tamu);
            json = json.put("kode",kode);
            json = json.put("nama",nama);
            json = json.put("email",email);
            json = json.put("hp",hp);
            if(tambah)
                json = json.put("tambah","yes");
            else
                json = json.put("tambah","no");
            return json;
        }catch (Exception e){
            return new JSONObject();
        }
    }

    public static JSONObject puthadir(String id, String nama, String kode, String status){
        try {
            JSONObject json = new JSONObject(hadir);
            json = json.put("id",id);
            json = json.put("kode",kode);
            json = json.put("nama",nama);
            json = json.put("status","");
            return json;
        }catch (Exception e){
            return new JSONObject();
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public static void showToast(String pesan, Context c) {
        Toast t = Toast.makeText(c, pesan, Toast.LENGTH_LONG);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();

    }

    private static final String PICASSO_CACHE = "picasso-cache";

    public static void clearCache(Context context) {
        final File cache = new File(
                context.getApplicationContext().getCacheDir(),
                PICASSO_CACHE);
        if (cache.exists()) {
            deleteFolder(cache);
        }
    }

    public static double getZonaWaktu() {
        Calendar now = new GregorianCalendar();
        int gmtOffset = now.getTimeZone().getOffset(now.getTimeInMillis());
        return gmtOffset / 3600000;
    }

    public static Bitmap getScaledBitmap(String path, int newSize) {
        File image = new File(path);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inInputShareable = true;
        options.inPurgeable = true;

        BitmapFactory.decodeFile(image.getPath(), options);
        if ((options.outWidth == -1) || (options.outHeight == -1))
            return null;

        int originalSize = (options.outHeight > options.outWidth) ? options.outHeight
                : options.outWidth;

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / newSize;

        Bitmap scaledBitmap = BitmapFactory.decodeFile(image.getPath(), opts);
        return scaledBitmap;
    }

    private static void deleteFolder(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteFolder(child);
        }
        fileOrDirectory.delete();
    }

    public static ArrayList getLinks(String text) {
        ArrayList links = new ArrayList();

        String regex = "\\(?\\b(https?://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        while (m.find()) {
            String urlStr = m.group();
            if (urlStr.startsWith("(") && urlStr.endsWith(")")) {
                urlStr = urlStr.substring(1, urlStr.length() - 1);
            }
            links.add(urlStr);
        }
        return links;
    }

    public static String getBulan(int month) {
        month++;
        switch (month) {
            case 1:
                return "Jan";
            case 2:
                return "Feb";
            case 3:
                return "Mar";
            case 4:
                return "Apr";
            case 5:
                return "May";
            case 6:
                return "Jun";
            case 7:
                return "Jul";
            case 8:
                return "Aug";
            case 9:
                return "Sep";
            case 10:
                return "Oct";
            case 11:
                return "Nov";
            case 12:
                return "Dec";
            default:
                return "Invalid";
        }
    }



    public static String milisToTanggal(long waktu) {
        return milisToTanggal(waktu, false);
    }

    public static String getRealPathFromURI(Uri contentUri, Context c) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = c.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }


    public static String milisToTanggal(long waktu, boolean bulan) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(waktu);
        if (bulan)
            return cal.get(Calendar.DAY_OF_MONTH) + " " + getBulan(cal.get(Calendar.MONTH)) + " " +
                    cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
        else
            return cal.get(Calendar.DAY_OF_MONTH) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" +
                    cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
    }


    public static void log(String txt) {
        Log.d("BantenKuliner",  "----------");
        Log.d("BantenKuliner", txt + " ");
        Log.d("BantenKuliner",  "----------");
    }

    public static void deleteDirectoryTree(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteDirectoryTree(child);
            }
        }

        fileOrDirectory.delete();
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String milisToTanggalLahir(long waktu) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(waktu);
        return cal.get(Calendar.DAY_OF_MONTH) + " " + getBulan(cal.get(Calendar.MONTH));
    }

    public static String milisToTanggalKegiatan(long waktu) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(waktu);
        return cal.get(Calendar.DAY_OF_MONTH) + " " + getBulan(cal.get(Calendar.MONTH)) + " " + cal.get(Calendar.YEAR);
    }

    public static byte[] hexToBytes(String hex) {
        String HEXINDEX = "0123456789abcdef";
        int l = hex.length() / 2;
        byte data[] = new byte[l];
        int j = 0;

        for (int i = 0; i < l; i++) {
            char c = hex.charAt(j++);
            int n, b;

            n = HEXINDEX.indexOf(c);
            b = (n & 0xf) << 4;
            c = hex.charAt(j++);
            n = HEXINDEX.indexOf(c);
            b += (n & 0xf);
            data[i] = (byte) b;
        }

        return data;
    }

    public static String padString(String source) {
        char paddingChar = ' ';
        int size = 16;
        int padLength = size - source.length() % size;

        for (int i = 0; i < padLength; i++) {
            source += paddingChar;
        }

        return source;
    }

    public static String enkripsi(String text)  throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException, IOException {
        String iv = "fedcba9876543210";
        IvParameterSpec ivspec;
        KeyGenerator keygen;
        Key key;

        ivspec = new IvParameterSpec(iv.getBytes());

        keygen = KeyGenerator.getInstance("AES");
        keygen.init(128);
        key = keygen.generateKey();

        SecretKeySpec keyspec = new SecretKeySpec(key.getEncoded(), "AES");

        Cipher cipher;
        byte[] encrypted;

        cipher = Cipher.getInstance("AES/CBC/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
        encrypted = cipher.doFinal(padString(text).getBytes());
        return new String(encrypted);
    }

    public static String dekripsi(String code) throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException, IOException {
        String iv = "fedcba9876543210";
        IvParameterSpec ivspec;
        KeyGenerator keygen;
        Key key;

        ivspec = new IvParameterSpec(iv.getBytes());

        keygen = KeyGenerator.getInstance("AES");
        keygen.init(128);
        key = keygen.generateKey();

        SecretKeySpec keyspec = new SecretKeySpec("L7ucmQIe6%2F4268".getBytes(), "AES");

        Cipher cipher;
        byte[] decrypted;

        cipher = Cipher.getInstance("AES/CBC/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
        decrypted = cipher.doFinal(hexToBytes(code));
        return decrypted.toString();
    }
     private static final String KEY = "57238004e784498bbc2f8bf984565090";

    public static String encrypt(final String plaintext) throws GeneralSecurityException {
        SecretKeySpec sks = new SecretKeySpec(hexStringToByteArray(KEY), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, sks, cipher.getParameters());
        byte[] encrypted = cipher.doFinal(plaintext.getBytes());
        return byteArrayToHexString(encrypted);
    }

    public static byte[] hexStringToByteArray(String s) {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

    public static String byteArrayToHexString(byte[] b) {
        StringBuilder sb = new StringBuilder(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            int v = b[i] & 0xff;
            if (v < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(v));
        }
        return sb.toString().toUpperCase();
    }

    public static float distFrom(float lat1, float lng1, float lat2, float lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

    public static String distFromTxt(float lat1, float lng1, float lat2, float lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);
        if(dist>1000){
            return new DecimalFormat("#,##").format(dist/1000)+" km";
        }else{
            return dist+" m";
        }
    }

    public static boolean isMockSettingsON(Context context) {
        // returns true if mock location enabled, false if not enabled.
        if (Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ALLOW_MOCK_LOCATION).equals("0"))
            return false;
        else
            return true;
    }

}
