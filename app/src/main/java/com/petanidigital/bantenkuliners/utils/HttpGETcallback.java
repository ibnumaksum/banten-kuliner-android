package com.petanidigital.bantenkuliners.utils;

/**
 * Created by ibnumaksum on 3/24/15.
 */
public interface HttpGETcallback {

    public void httpResultSuccess(String result);

    public void httpResultError(String result, int statusCode);
}
