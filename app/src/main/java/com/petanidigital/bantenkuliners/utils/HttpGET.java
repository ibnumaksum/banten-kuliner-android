package com.petanidigital.bantenkuliners.utils;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;


/**
 * Created by ibnumaksum on 3/24/15.
 */
public class HttpGET {
    private static AsyncHttpClient httpclient = null;

    public static void doGetRequest(final String url, final HttpGETcallback callback) {

        doGetRequestHeader(url, null, callback);
    }
    public static void doGetRequestHeader(final String url, String[] header, final HttpGETcallback callback) {
        try {
            if (httpclient == null)
                httpclient = new AsyncHttpClient();
            httpclient.addHeader("APLIKASI","BAKUL");
            httpclient.setMaxRetriesAndTimeout(3,30000);
            if(header!=null) {
                int jml = header.length;
                for(int n=0;n<jml;n++){
                    String[] temp = header[n].split("<:>");
                    httpclient.addHeader(temp[0],temp[1]);
                }
            }
            httpclient.get(url, new AsyncHttpResponseHandler() {

                @Override
                public void onStart() {
                    Utils.log("GET " + url);
                }

                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                    callback.httpResultSuccess(new String(responseBody));
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    try {
                        Utils.log(statusCode + " : " + new String(responseBody));
                        callback.httpResultError(new String(responseBody), statusCode);
                    } catch (Exception ex) {
                    }
                }

                @Override
                public void onRetry(int retryNo) {
                    Utils.log("Ulang " + retryNo);
                }
            });
        } catch (Exception e) {
            callback.httpResultError(e.getMessage(), 0);
        }
    }

    public static void doPostRequest(final String url, RequestParams params, final HttpGETcallback callback) {
        doPostRequestHeader(url,params,null,callback);
    }

    public static void doPostRequestHeader(final String url, RequestParams params, String[] header, final HttpGETcallback callback) {
        try {
            if (httpclient == null)
                httpclient = new AsyncHttpClient();
            httpclient.addHeader("APLIKASI","BAKUL");
            if(header!=null) {
                int jml = header.length;
                for(int n=0;n<jml;n++){
                    String[] temp = header[n].split("<:>");
                    httpclient.addHeader(temp[0],temp[1]);
                }
            }
            httpclient.post(url, params, new AsyncHttpResponseHandler() {

                @Override
                public void onStart() {
                    Utils.log("POST " + url);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                    callback.httpResultSuccess(new String(response));
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                    try {
                        Utils.log(statusCode + " : " + new String(errorResponse));
                        callback.httpResultError(new String(errorResponse), statusCode);
                    } catch (Exception ex) {
                    }
                }

                @Override
                public void onRetry(int retryNo) {
                    Utils.log("Ulang " + retryNo);
                }
            });
        } catch (Exception e) {
            callback.httpResultError(e.getMessage(), 0);
        }
    }

    public static void batal() {
        httpclient.cancelAllRequests(true);
    }

}
