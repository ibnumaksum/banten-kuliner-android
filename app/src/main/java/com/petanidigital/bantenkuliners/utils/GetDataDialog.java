package com.petanidigital.bantenkuliners.utils;

import android.app.Dialog;
import android.content.Context;
import android.widget.ProgressBar;

import com.loopj.android.http.RequestParams;
import com.petanidigital.bantenkuliners.R;

public class GetDataDialog extends Dialog {
    private String url, hasil;
    private ProgressBar progressBar;

    public GetDataDialog(Context c) {
        super(c);
        setContentView(R.layout.dialog_get_data);
    }

    public void geturl(String url) {
        geturlParam(url, null);
    }

    public void geturlParam(String url, String[] param) {
        this.url = url;
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setIndeterminate(true);
        show();
        HttpGET.doGetRequestHeader(url, param, new HttpGETcallback() {
            @Override
            public void httpResultSuccess(String result) {
                hasil = result;
                dismiss();
            }

            @Override
            public void httpResultError(String result, int statusCode) {
                hasil = "Error: " + result;
                dismiss();
            }
        });
    }

    public void posturlParam(String url, RequestParams params, String[] header) {
        this.url = url;
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setIndeterminate(true);
        show();
        HttpGET.doPostRequestHeader(url, params,header, new HttpGETcallback() {
            @Override
            public void httpResultSuccess(String result) {
                hasil = result;
                dismiss();
            }

            @Override
            public void httpResultError(String result, int statusCode) {
                hasil = "Error: " + result;
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        HttpGET.batal();
    }

    public String getHasil() {
        return hasil;
    }
}
