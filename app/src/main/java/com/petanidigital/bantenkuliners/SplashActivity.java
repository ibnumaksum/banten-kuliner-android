package com.petanidigital.bantenkuliners;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ayz4sci.androidfactory.permissionhelper.PermissionHelper;

import pl.tajchert.nammu.PermissionCallback;

public class SplashActivity extends AppCompatActivity {
    PermissionHelper permissionHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        permissionHelper = PermissionHelper.getInstance(this);
        permissionHelper.verifyPermission(
                new String[]{"Fine location"},
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                new PermissionCallback() {
                    @Override
                    public void permissionGranted() {
                        startActivity(new Intent(SplashActivity.this,PropinsiActivity.class));
                        finish();
                    }

                    @Override
                    public void permissionRefused() {
                        startActivity(new Intent(SplashActivity.this,PropinsiActivity.class));
                        finish();
                    }
                }
        );

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        permissionHelper.onActivityResult(requestCode, resultCode, data);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onDestroy() {
        permissionHelper.finish();
        super.onDestroy();
    }

}
