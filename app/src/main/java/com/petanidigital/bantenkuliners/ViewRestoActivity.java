package com.petanidigital.bantenkuliners;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.petanidigital.bantenkuliners.utils.HttpGET;
import com.petanidigital.bantenkuliners.utils.HttpGETcallback;
import com.petanidigital.bantenkuliners.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

public class ViewRestoActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener {
    double latt,lonn;
    TextView nama,alamat,deskripsi;

    SliderLayout slider;
    JSONObject jsonResto;
    int maxWidth,maxHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_resto);

        nama = (TextView)findViewById(R.id.txtNama);
        alamat = (TextView)findViewById(R.id.txtAlamat);
        deskripsi = (TextView)findViewById(R.id.txtDeskripsi);
        TextView txtJarak = (TextView)findViewById(R.id.txtJarak);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        maxHeight = displayMetrics.heightPixels;
        maxWidth = displayMetrics.widthPixels;

        try{
            jsonResto = new JSONObject(getIntent().getStringExtra("data"));
            nama.setText(jsonResto.getString("nama"));
            alamat.setText(jsonResto.getString("alamatLengkap"));
            deskripsi.setText(jsonResto.getString("deskripsi"));
            if(getIntent().hasExtra("lat") && getIntent().hasExtra("lon")) {
                latt = jsonResto.getDouble("lat");
                lonn = jsonResto.getDouble("lon");

                txtJarak.setText(Utils.distFromTxt((float) getIntent().getDoubleExtra("lat",0), (float)  getIntent().getDoubleExtra("lon",0),
                        (float) latt, (float) lonn));

            }else{
                txtJarak.setText("");
            }
        }catch (Exception e){
            Utils.log(e.toString());
        }

        slider = (SliderLayout) findViewById(R.id.slider);
        slider.stopAutoCycle();
        slider.setDuration(5000);
        try{
            String idLOkasi = jsonResto.getString("id_lokasi");
            HttpGET.doGetRequest(Utils.apiurl+"apa=slide&lokasi="+idLOkasi, new HttpGETcallback() {
                @Override
                public void httpResultSuccess(String result) {
                    parsingSlide(result);
                }

                @Override
                public void httpResultError(String result, int statusCode) {

                }
            });
        }catch (Exception e){

        }
    }

    public void bukaMaps(View view){
        MapsFragment.newInstance(latt+"<lokasi>"+lonn,nama.getText().toString()).show(getSupportFragmentManager(),"");
    }

    public void cariJalur(View view){
        try{
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("google.navigation:q="+latt+","+lonn));
            startActivity(intent);
        }catch (Exception e){
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?daddr="+latt+","+lonn));
            startActivity(intent);
        }
    }

    public void shareIt(View view) throws Exception{
        String shareBody = Utils.regurl+"?tampilkan="+jsonResto.getString("id_lokasi");
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, nama.getText().toString());
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent,"Bagikan lewat:"));
    }

    public void call2Order(View view){
        try{
            startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("tel:"+jsonResto.getString("noTelepon"))));
        }catch (Exception e){
            Utils.log(e.getMessage());
            Utils.showToast("Tidak ada nomor Telepon",this);
        }
    }

    public void parsingSlide(String result){
        try {
            JSONArray json = new JSONArray(result);
            int jml = json.length();
            int height = 50;
            slider.removeAllSliders();
            try{
                String imgUrl = Utils.imgUrl+jsonResto.getString("foto_lokasi");
                Utils.log(imgUrl);
                DefaultSliderView textSliderView = new DefaultSliderView(ViewRestoActivity.this);
                textSliderView
                        .image(imgUrl)
                        //.setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                        .setOnSliderClickListener(ViewRestoActivity.this);
                Bundle bundle = new Bundle();
                bundle.putString("url", imgUrl);
                textSliderView.bundle(bundle);
                slider.addSlider(textSliderView);
            }catch (Exception e){
                Utils.log(e.toString());
            }
            for (int n = 0; n < jml; n++) {
                JSONObject obj = json.getJSONObject(n);
                Utils.log(obj.toString());
                if (obj.has("judulGambar") && obj.getString("judulGambar").length()>2) {
                    String img = Utils.imgUrl+obj.getString("pathGambar");
                    TextSliderView textSliderView = new TextSliderView(ViewRestoActivity.this);
                    textSliderView
                            .image(img)
                            //.setScaleType(BaseSliderView.ScaleType.CenterInside)
                            .setOnSliderClickListener(ViewRestoActivity.this);
                    if(obj.has("judulGambar"))
                        textSliderView.description(obj.getString("judulGambar"));
                    textSliderView.bundle(new Bundle());
                    if (obj.has("urlGambar") && obj.getString("urlGambar").length()>2) {
                        textSliderView.getBundle().putString("url", obj.getString("judulGambar"));
                    } else {
                        textSliderView.getBundle().putString("url", img);
                    }
                    slider.addSlider(textSliderView);

                    int h = 432;
                    if (height < h) {
                        height = h;
                        float ratioBitmap = (float) 768 / (float) height;
                        float ratioMax = (float) maxWidth / (float) maxHeight;
                        int finalWidth = maxWidth;
                        int finalHeight = maxHeight;
                        if (ratioMax > 1) {
                            finalWidth = (int) ((float) maxHeight * ratioBitmap);
                        } else {
                            finalHeight = (int) ((float) maxWidth / ratioBitmap);
                        }
                        height = finalHeight;
                    }

                } else {
                    String img = Utils.imgUrl+obj.getString("pathGambar");
                    DefaultSliderView textSliderView = new DefaultSliderView(ViewRestoActivity.this);
                    textSliderView
                            .image(img)
                            //.setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                            .setOnSliderClickListener(ViewRestoActivity.this);
                    Bundle bundle = new Bundle();
                    if (obj.has("judulGambar") && obj.getString("judulGambar").length()>2) {
                        bundle.putString("url", obj.getString("judulGambar"));
                    } else {
                        bundle.putString("url", img);
                    }
                    textSliderView.bundle(bundle);
                    slider.addSlider(textSliderView);

                    int h = 432;
                    if (height < h) {
                        height = h;
                        float ratioBitmap = (float) 768 / (float) height;
                        float ratioMax = (float) maxWidth / (float) maxHeight;
                        int finalWidth = maxWidth;
                        int finalHeight = maxHeight;
                        if (ratioMax > 1) {
                            finalWidth = (int) ((float) maxHeight * ratioBitmap);
                        } else {
                            finalHeight = (int) ((float) maxWidth / ratioBitmap);
                        }
                        height = finalHeight;
                    }

                }
                if(jml>0) {
                    Utils.log("height " + height);
                    //slider.getLayoutParams().height = height;
                    slider.setPresetTransformer(SliderLayout.Transformer.Stack);
                    slider.setPresetIndicator(SliderLayout.PresetIndicators.Right_Bottom);
                    // slider.setCustomAnimation(new DescriptionAnimation());
                    slider.setDuration(5000);
                    slider.startAutoCycle();
                }else{
                    slider.stopAutoCycle();
                    slider.setVisibility(View.GONE);
                }
            }
        }catch (Exception e){
            Utils.log(e.getMessage());
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        if (slider.getBundle() != null && slider.getBundle().containsKey("url")) {
            String url = slider.getBundle().getString("url");
            Intent intent = new Intent(this,ViewPhotoActivity.class);
            intent.putExtra("url",url);
            startActivity(intent);
        }else{
            Utils.log("kosong");
        }
    }

    @Override
    public void onPause() {
        if(slider!=null)
            slider.stopAutoCycle();
        super.onPause();
    }

    @Override
    public void onResume() {
        if(slider!=null)
            slider.startAutoCycle();
        super.onResume();
    }

}
